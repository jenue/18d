---
title: Bonnes pratiques Git
date: "2021-03-30"
description: "Explication d'une méthode pour travailler proprement avec Git et éviter au maximum les conflits de commits."
tags: ["git", "workflow", "gitlab"]
---

![Gitlab Logo](./../../../assets/pull-rebase-global.png)

## Configuration globale

Toujours réaliser des `git pull` avec `rebase` plutôt que `merge`, afin de garder un historique plus propre.
```
git config --global pull.rebase true
```

Créer un alias Git pour push avec une merge request Gitlab.

Alias : Push Merge Request Master
```
git config --global alias.pmrm "push -o merge_request.create -o merge_request.target=master"
```

Alias : Push Merge Request Develop
```
git config --global alias.pmrd "push -o merge_request.create -o merge_request.target=develop"
```

## Workflow

Je commence une nouvelle feature
```
git checkout origin brancheSource
git pull origin brancheSource
git checkout -b feature--branchName
```

Je commit sur ma nouvelle branche fréquemment, essayer de segmenter au maxium les commits avec des messages clairs.
```
git add pathFile pathFile2...
git commit -m "#IdIssue message du commit"
```

Je rebase au moins une fois par jour la branche source
```
git rebase origin/brancheSource
```

Dès que j'ai terminé mon développement, je rebase et push avec une merge request Gitlab (exemple de merge request si la branche source est develop)
```
git rebase origin/brancheSource
git pmrd origin feature--branchName
```

Note : Il est fortement conseillé d'avoir une pipeline CI/CD de vérification du code (par ex : phpcs, phpstan, phpunits) qui se lance à chaque push concerné par une merge request.

Après vérification des changements apportés depuis l'interface de la merge request Gitlab, j'assigne cette dernière au lead tech du projet pour code review et validation.

## Références

- La [documentation en français](https://www.atlassian.com/fr/git/tutorials/syncing) de Atlassian Bitbucket


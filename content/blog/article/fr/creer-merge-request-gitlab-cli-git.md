---
title: Créer une merge request Gitlab avec la cli de Git
date: "2021-03-12"
description: "Utiliser simplement la commande Git pour créer une merge request Gitlab."
tags: ["git", "gitlab"]
---

![Gitlab Logo](./../../../assets/gitlab-violet.png)


## Contexte

J’ai l’habitude de créer une merge request comme certainement la plupart des développeurs depuis l’interface de Gitlab. Cependant pour certains cas d’utilisations par exemple l’industrialisions, il peut s’avérer plus utile d’utiliser simplement la cli de Git pour réaliser cette opération.

## Solution

Il est effectivement possible avec les options Git de créer une merge request depuis son terminal à condition d’avoir une version de Git >= à : `2.10`

Note : A partir de la version `2.18` on peut utiliser le raccourci de l'option `-o` au lieu de  `--push-option`.

```
git --version
```

#### Exemple pour créer une merge request
```
git push -o merge_request.create -o merge_request.target=master
```
Cette commande permet de créer une MR depuis la branche en cours vers celle de master.


#### Les options disponibles

```
git push -o merge_request.create
```
Créer une nouvelle merge request.

```
git push -o merge_request.target=targetBranch
```
Indiquer la branche cible
.
```
git push -o merge_request.merge_when_pipeline_succeeds
```
Lancer un pipeline dès que la merge est validée.

```
git push -o merge_request.remove_source_branch
```
Supprimer la branche source que la merge est validée.

```
git push -o merge_request.title="Titre"
```
Indiquer un titre à la merge request.

```
git push -o merge_request.description="Description"
```
Indiquer une description à la merge request.

```
git push -o merge_request.label="Label1"
```
Cette option peut être indiquée plusieurs fois si plusieurs labels, si un label n'existe pas, alors il est créé.

#### Astuce

La commande pour créer une merge request avec Git est vraiment très longue à écrire avec toutes ces options. On peut utiliser les alias pour palier à ce problème.

Exemple d'alias à mettre en place
```
git config --global alias.mwps "push -o merge_request.create -o merge_request.target=master"
```

Créer une merge request avec Git est tout de suite beaucoup plus simple et rapide.
```
git mwps origin <local-branch-name>
```

## Références

- La documentation officielle de Gitlab sur les [Push options](https://docs.gitlab.com/ee/user/project/push_options.html)

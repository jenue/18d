---
title: Génération d'instances statiques Drupal par merge request avec Gitlab
date: "2020-12-19"
description: "Création d'environnements statiques Gitlab Pages par merge request avec Gitlab CI."
tags: ["drupal", "gitlab"]
---

![Gitlab Logo](./../../../assets/gitlab-logo.png)

## Contexte

Gitlab est outil très complet sur beaucoup de points, plus besoin de le présenter.

Une récente vidéo de [Philippe Charrière](https://www.twitch.tv/k33g_org) sur compte Twitch m'a donnée envie d'utiliser la fonctionnalité Gitlab pages.

Pour rappel, Gitlab permet d'héberger des sites statiques, il détecte les données envoyées dans le dossier `public` et génère une URL par dépôt.

Le but de cet article est de créer un site statique par branche avec le déclenchement de merge request sur Gitlab. C'est le même mécanisme que si vous utilisez un PAAS comme Platformsh ou Beanstalk mais ici c'est totalement gratuit et statique.


## Solution

#### Prerequis
- un projet Drupal 8 ou 9 sur votre machine d'installé
- le module [Tome](https://www.drupal.org/project/tome) d'installer et paramétrer (les valeurs par défaut suffisent)
- le projet Drupal versionné sur un dépôt Gitlab
- un [Runner Gitlab](https://docs.gitlab.com/runner/) disponible pour votre projet si vous êtes sur une instance de Gitlab auto hebergé

#### Créer le fichier gitlab-ci

Créer le fichier `.gitlab-ci.yml` à la racine de votre projet avec les valeurs suivantes :

```yml

variables:
  URL_GITLAB_PAGE: "gitlab.io"

stages:
  - publish

# create the review environment
pages:
  stage: publish
  script:
    - find ./html/ -type f -exec sed -i -e 's+href="\/+href="https\:\/\/'"${CI_PROJECT_ROOT_NAMESPACE}"'.${URL_GITLAB_PAGE}\/'"${CI_PROJECT_NAME}"'\/'"${CI_COMMIT_REF_NAME}"'\/+g' {} \;
    - find ./html/ -type f -exec sed -i -e 's+src="\/+src="https\:\/\/'"${CI_PROJECT_ROOT_NAMESPACE}"'.${URL_GITLAB_PAGE}\/'"${CI_PROJECT_NAME}"'\/'"${CI_COMMIT_REF_NAME}"'\/+g' {} \;
    - cp -R ./html/ ./public/
    - ls ./public
    - mv ./public/html/ ./public/${CI_COMMIT_REF_NAME}/
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_MERGE_REQUEST_IID
  environment:
    name: preview/${CI_COMMIT_REF_NAME}
    url: https://${CI_PROJECT_ROOT_NAMESPACE}.${URL_GITLAB_PAGE}/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}/index.html
    on_stop: pages:stop

# remove the review environment
pages:stop:
  stage: publish
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  allow_failure: true
  environment:
    name: preview/${CI_COMMIT_REF_NAME}
    action: stop
  script:
    - echo "stop environnement"
```

#### Activer la CI sur le projet
Push du fichier gitlab-ci
```shell
git add .gitlab-ci.yml
git commit -m "add gitlab-ci.yml file"
git push origin master # branche master ici pour l'exemple
```

#### Création d'un nouveau site statique
Créer une nouvelle branche de feature
```shell
git checkout -b MA_NOUVELLE_BRANCHE
```

Générer le site statique avec le module Drupal `Tome`
```shell
drush tome:export -y
drush tome:static
```

Push du dossier `html` créé à la racine du projet
```shell
git add html
git commit -m "add static site of MA_NOUVELLE_BRANCHE branch"
git push origin MA_NOUVELLE_BRANCHE
```

Créer une merge request sur Gitlab entre la nouvelle branche et votre branche de master. Cette opération va lancer le script de CI pour déplacer votre site statique dans le dossier `public` (document root de votre GitLab Pages).

Et voilà, l'environnement est créé après quelques secondes ;)

La liste des environnements disponibles se situe dans l'interface Gitlab de votre projet : `Opérations -> Environnements`.

Les environnements peuvent être stoppés à tout moment indépendamment en cliquant sur le bouton rouge ou en cliquant sur l'action manuelle de la CI.

À l'avenir, un environnement sera donc créé par merge request. J'ai mis en place cette procédure essentiellement pour de la recette front en interne.

Attention quand même à bien vérifier les formulaires ou autre fonctionnalités qui ne pourrait ne plus fonctionner sur le site statique.

## Références

- Module Drupal [Tome](https://www.drupal.org/project/tome)

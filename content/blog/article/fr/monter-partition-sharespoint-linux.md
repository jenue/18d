---
title: Monter une partition SharePoint sur Linux
date: "2020-12-07"
description: "Liste d'opérations qui permettent de monter un disque SharePoint sur Linux."
tags: ["linux", "microsoft", "bash"]
---

![Cloud Logo](./../../../assets/cloud.jpeg)

## Contexte

Connecter des outils `Microsoft` sur une machine `Linux` c'est jamais simple.

Voici une liste d'opérations qui permettent de monter un disque SharePoint au démarrage avec les droits sans lancer une seule ligne de commande. Je l'utilise depuis quelques semaines sur mon Ubuntu 20.04 LTS.

## Solution

Créer un dossier dans mnt qui sera utilisée disque de montage
```shell
sudo mkdir /mnt/sharepoint
```

Installation de davfs2 qui va nous permettre de monter le partage WebDAV
```shell
sudo apt install davfs2
```

Nous avons besoin de récupérer les cookies utilisés pour authentifier un utilisateur sur Sharepoint
```shell
git clone https://gitnet.fr/deblan/office365-oauth2-authenticator.git office365-oauth2-authenticator && cd $_
npm install
cp login_example login
chmod +x login
```

Remplacer par vos données dans le fichier login
```shell
MS_OFFICE365_SITE
MS_OFFICE365_LOGIN
MS_OFFICE365_PASSWORD
```

Lancer le fichier login pour récupérer cookies de sharepoint
```shell
./login
```

Ajouter en remplacement les XXX par les cookies récupérés dans le fichier davfs2.conf
```shell
vim /etc/davfs2/davfs2.conf
```
```shell
[/mnt/sharepoint/]
ask_auth 0
add_header Cookie rtFa=XXX;FedAuth=XXX
```

Ajouter cette ligne à la fin de votre fichier fstab afin de monter le volume
```shell
https://XXX.sharepoint.com/sites/XXX/ /mnt/sharepoint davfs defaults,rw,user,_netdev 0 0
```

Ajouter son user au groupe davfs2
```shell
sudo adduser $USER davfs2
```

Editer le fichier de configuration profil utilisateur linux
```shell
vim ~/.profile
```

Ajouter ces lignes pour monter la partition sharepoint au démarrage
```shell
if ! grep -q "mnt/sharepoint" /proc/mounts
then
mount https://XXX.sharepoint.com/sites/XXX/
fi
```

Les données partagées de Sharepoint sont disponibles ici
```shell
cd '/mnt/sharepoint/Shared Documents'
```

Créer un script à exécuter dans une crontab afin de régénérer les cookies périodiquement
```shell
vim ~/regenerate-cookies-sharepoint.sh
```
```shell
#!/bin/bash

DATAS=$(sh login | jq .)
DAVCONF="/etc/davfs2/davfs2.conf"
RTFA=$(echo $DATAS | jq -r '.rtFa')
echo 'rtFa : ' $RTFA
FEDAUTH=$(echo $DATAS | jq -r '.FedAuth')
echo 'FedAuth : ' $FEDAUTH
sudo sed -i 's|rtFa\=.*\;FedAuth|rtFa\='"$RTFA"'\;FedAuth|g' $DAVCONF
sudo sed -i 's|FedAuth\=.*|FedAuth\='"$FEDAUTH"'|g' $DAVCONF
```

Ce script doit être lancé depuis la racine du dépot : office365-oauth2-authenticator
```shell
cd office365-oauth2-authenticator && sh ~/regenerate-cookies-sharepoint.sh
```

Dès à présent, la partition est montée à chaque démarrage de votre machine et que les cookies sont regénérés périodiquement. Vous pouvez accéder à vos documents partagées Sharepoint tout le temps en mode lecture/écriture.

Note : si vous perdez l'accessibilité au disque sharepoint, vous pouvez récupérer les cookies et remonter le disque
```shell
sudo umount /mnt/sharepoint
cd office365-oauth2-authenticator && sh ~/regenerate-cookies-sharepoint.sh
mount -a
```

## Références

- Installer [Jq](https://stedolan.github.io/jq/download/)
- Installer [davfs2](https://doc.ubuntu-fr.org/davfs2)



---
title: Xdebug 3 sur VScode
date: "2020-12-12"
description: "Nouvelle version de Xdebug compatible avec VScode."
tags: ["vscode", "xdebug", "php"]
---

![Xdebug Logo](./../../../assets/xdebug.png)

## Contexte

Xdebug 3 est maintenant en sorti depuis le 25 novembre avec son lot d’améliorations.

Petite particularité à prendre compte, le port d'écoute par défaut a changé. Ce n'est plus `9000` mais `9003`.

## Solution

Pour faire fonctionner Xdebug 3 sur Vscode :

#### Vérifier que xdebug est bien chargé dans le conf de php
```
php -v | grep Xdebug
```

Si rien en retour ou encore en version antérieure à 3, alors installer xdebug
documentation d'installation : https://xdebug.org/docs/install

#### Modifier le fichier de conf launch.json de VSCode
```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Listen for XDebug",
            "type": "php",
            "request": "launch",
            "port": 9003,
        }
    ]
}
```

#### Récupérer le chemin du fichier php.ini
```shell
php --ini
```

#### Modifier le php.ini
```shell
[XDebug]
xdebug.mode = debug
xdebug.start_with_request = yes
xdebug.discover_client_host = true
```

#### Redémarrer votre php-fpm (ex pour php7.4)
```shell
service php7.4-fpm restart
```

Xdebug 3 est mainteant compatible et fonctionnel avec VSCode.

## Références

- Installer [Xdebug](https://xdebug.org/docs/install)

/**
 * Bio component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Image from "gatsby-image"

const Bio = () => {
  const data = useStaticQuery(graphql`
    query BioQuery {
      avatar: file(absolutePath: { regex: "/profile-pic.jpg/" }) {
        childImageSharp {
          fixed(width: 50, height: 50, quality: 90) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      site {
        siteMetadata {
          author {
            name
            summary
          }
          social {
            linkedin
            gitlab
            drupal
            github
          }
        }
      }
    }
  `)

  // Set these values by editing "siteMetadata" in gatsby-config.js
  const author = data.site.siteMetadata?.author
  const social = data.site.siteMetadata?.social

  const avatar = data?.avatar?.childImageSharp?.fixed

  return (
    <div className="bio">
      {avatar && (
        <Image
          fixed={avatar}
          alt={author?.name || ``}
          className="bio-avatar"
          imgStyle={{
            borderRadius: `50%`,
          }}
        />
      )}
      {author?.name && (
        <p>
          Blog de <strong>{author.name}</strong> | {author?.summary || null}
          {` | `}
          <a href={`https://fr.linkedin.com/in/${social?.linkedin || ``}`}>
            Linkedin
          </a>
          {` `}
           -
          {` `}
          <a href={`https://gitlab.com/${social?.gitlab || ``}`}>
            Gitlab
          </a>
          {` `}
           -
          {` `}
          <a href={`https://github.com/${social?.github || ``}`}>
            Github
          </a>
          {` `}
           -
          {` `}
          <a href={`https://www.drupal.org/u/${social?.drupal || ``}`}>
            Drupal
          </a>
        </p>
      )}
    </div>
  )
}

export default Bio
